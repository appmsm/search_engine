require "benchmark"
desc "Load document data  to database"
	task :load_review_data => :environment do
		review = Review.new
		puts "loading data started -----"
		File.open("public/sample.txt",'r').each_line do |line|

			if line.include?("product/productId: ")
				review.product_id = line.gsub("product/productId: ","").gsub("\n","")
				next
			end

			if line.include?("review/userId: ")
				review.user_id = line.gsub("review/userId: ","").gsub("\n","")
				next
			end
			if line.include?("review/profileName: ")
				review.profile_name = line.force_encoding('iso8859-1').encode('utf-8').gsub("review/text: ","").gsub("review/profileName: ","").gsub("\n","")
				next
			end
			if line.include?("review/helpfulness: ")
				review.helpfulness = line.gsub("review/helpfulness: ","").gsub("\n","")
				next
			end
			if line.include?("review/score: ")
				review.score = line.gsub("review/score: ","").gsub("\n","").to_f
				next
			end
			if line.include?("review/time: ")
				time = line.gsub("review/time: ","").gsub("\n","")
				review.time = DateTime.strptime(time,"%s")
				next
			end
			if line.include?("review/summary: ")
				review.summary = line.force_encoding('iso8859-1').encode('utf-8').gsub("review/text: ","").gsub("review/summary: ","").gsub("\n","")
				next
			end
			if line.include?("review/text: ")
				review.text = line.force_encoding('iso8859-1').encode('utf-8').gsub("review/text: ","").gsub("\n","")
				next
			end
			if line == "\n"
				if !review.product_id.nil? and !review.user_id.nil?
					review.save
				end
				review = Review.new
			end
		end
		puts "data loaded into the system"
	end

desc "load words to cache"
  task :load_words_in_cache => :environment do
    puts "------loading words into hash started-------"
		time = Benchmark.measure do
	    Review.all.find_in_batches do |batch|
	      batch.each do |review|
	        tokens = TokenizeHelper.tokenize(review.text + " " + review.summary)
					tokens = StopWordHelper.remove_stop_words(tokens)
					# if needed stemming can also be done
	        tokens.each do |word|
	          if Rails.cache.exist?(word)
	            reviews = Rails.cache.read(word)
	            if !reviews.include?(review)
	              reviews << review
	            end
	            Rails.cache.write(word,reviews)
	          else
	            Rails.cache.write(word,[review])
	          end
					end
	      end
	    end
		end
		puts time
    puts "------loading words into cache ended-------"
  end
