module StopWordHelper
  def self.remove_stop_words(tokens)
    tokens.each do |token|
      if !Stopwords.valid?(token)
        tokens.delete(token)
      end
    end
  end
end
