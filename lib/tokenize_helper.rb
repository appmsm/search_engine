require 'tokenizer'
module TokenizeHelper
  def self.tokenize(string)
    de_tokenizer = Tokenizer::WhitespaceTokenizer.new
    de_tokenizer.tokenize(string)
  end
end
