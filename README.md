# README

requires:
ruby 2.5.1
postgres

Follow these steps:
```
git clone https://appmsm@bitbucket.org/appmsm/search_engine.git
git pull origin master
cd search_engine
bundle install
```
postgres database is used to store review data for future reference. For querying inmemory is used!

```
rake db:create db:migrate
```
One time loading of data and caching is required , tokenize, stop words removal are done before loading to cache

```
rake load_review_data
rake load_words_in_cache
```

That's it. you are set. Start the server  
```
rails s
```
click `http://localhost:3000/search`

Enter the input as 
`query1, query 2`
