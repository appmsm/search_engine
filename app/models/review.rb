class Review < ApplicationRecord
  def self.search_results(query)
    result = []
    Rails.logger.info 'input query - #{query}'
    if !query.empty?
  		review_hash = {}
  		query.each do |token|
  			reviews = Rails.cache.read(token)
  			if !reviews.nil?
  				reviews.each do |review|
            if review_hash.key?(review)
              review_hash[review] = review_hash[review][0] + 1
            else
              review_hash[review] = [1, review.score]
            end
  				end
  			end
  		end
      # sorting based o the calculated score , if its same then check document score
  		result = review_hash.sort_by{|k, v| [-v[0], -v[1]]}.first(20).map{ |review| review[0] }
    end
    result
	end
end
