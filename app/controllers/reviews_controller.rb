class ReviewsController < ApplicationController
  def index
    @reviews =  Review.search_results(index_params)
    render 'index'
  end

  private

  def index_params
    params.permit(:query)
    params[:query].present? ? params[:query].split(',') : []
  end

end
